from apps.core.models import User
from rest_framework import serializers
from django_countries.fields import CountryField


# Serializer que rellena correctamente el campo CountryField
class SerializableCountryField(serializers.ChoiceField):
    def __init__(self, **kwargs):
        super(SerializableCountryField, self).__init__(choices=CountryField().choices)

    def to_representation(self, value):
        if value in ('', None):
            return '' # normally here it would return value. which is Country(u'') and not serialiable
        return super(SerializableCountryField, self).to_representation(value)


# Serializer para convertir los datos a json y validar datos.
class UserSerializer(serializers.ModelSerializer):

    country = SerializableCountryField(allow_blank=True)
    password = serializers.CharField(max_length=100, required=False)

    class Meta:
        model = User
        fields = [
            'pk',
            'name',
            'surnames',
            'email',
            'password',
            'country',
            'phonenumber',
            'postalCode',
        ]
        read_only_fields = ['pk']
