from rest_framework import generics, mixins
from apps.core.models import User
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponseBadRequest
from .serializers import UserSerializer


# Vista de API que crea o lista los usuarios dependiendo del tipo de llamada
# GET -> Lista usuarios
# POST -> Crea usuario
# Esta vista no requiere de ningún permiso y se pueden llamar sin estar autenticado.
# Al crear el usuario encripta nuestro password con el método de django
class UserAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    lookup_field = 'pk'
    serializer_class = UserSerializer
    permission_classes = []

    def get_queryset(self):
        return User.objects.all().exclude(username='admin')

    def post(self, request, *args, **kwargs):
        try:
            self.create(request, *args, **kwargs)
            user = User.objects.get(email=request.data['email'])
            user.set_password(request.data["password"])
            user.username = user.email
            user.save()
            return Response({"Success": "User created correctly"}, status=status.HTTP_201_CREATED)
        except Exception as e:
            return HttpResponseBadRequest(e)


# Vista de API para actualizar o borrar un usuario
# PUT -> Actualiza un usuario
# DELETE -> Borra un usuario
# Ambas llamadas requieren la primary key en la url
class UserRudView(generics.RetrieveUpdateDestroyAPIView):
    lookup_field = 'pk'
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.all()

    def get_object(self):
        pk = self.kwargs.get("pk")
        return get_object_or_404(User, pk=pk)
