from django.conf.urls import url, include
from .views import UserRudView, UserAPIView

app_name = "api-user"

urlpatterns = [
    url(r'^$', UserAPIView.as_view(), name="user-listcreate"),
    url(r'^(?P<pk>\d+)/$', UserRudView.as_view(), name="user-rud"),
]