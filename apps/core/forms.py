from django import forms
from .models import User
from bootstrap_modal_forms.forms import BSModalForm


# Formulario que se muestra en el modal de crear usuario
class UserForm(BSModalForm):

    class Meta:
        model = User
        fields = ('name', 'surnames', 'email', 'password', 'country', 'phonenumber', 'postalCode',)
        widgets = {
            'password': forms.PasswordInput(),
        }


# Formulario que se muestra en el modal de actualizar usuario (actualizar no permite el cambio de password)
class UserUpdateForm(BSModalForm):

    class Meta:
        model = User
        fields = ('name', 'surnames', 'email', 'country', 'phonenumber', 'postalCode',)
        widgets = {
            'password': forms.PasswordInput(),
        }


# Formulario que se muestra en el modal de loguear usuario
class UserLoginForm(BSModalForm):

    class Meta:
        model = User
        fields = ['email','password']
        widgets = {
            'password': forms.PasswordInput(),
        }