from django.db import models
from django import forms
from phonenumber_field.modelfields import PhoneNumberField
from django_countries.fields import CountryField
from django.contrib.auth.models import AbstractUser


# Nuestro modelo de usuario, es el que también se utiliza para autenticar los clientes
class User(AbstractUser):
    REQUIRED_FIELDS = ['password', 'name']
    USERNAME_FIELD = 'email'

    name = models.CharField(max_length=30, null=False, blank=False)
    surnames = models.CharField(max_length=30, null=False, blank=False)
    email = models.EmailField(max_length=60, unique=True)
    password = models.CharField(max_length=100, null=False, blank=False)
    country = CountryField()
    phonenumber = PhoneNumberField()
    postalCode = models.CharField(max_length=5)

    def __str__(self):
        return self.email
