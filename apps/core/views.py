from django.shortcuts import render, HttpResponse
from django.views.generic import TemplateView
from django.http import HttpResponseBadRequest
import requests
import json
from rest_framework.reverse import reverse as api_reverse
from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView
from .forms import UserForm, UserLoginForm, UserUpdateForm
from django.conf import settings
from .models import User
from django.contrib.auth import authenticate, login, logout


def get_absolute_url(view):
    return settings.API_URL + api_reverse(view)


# Vista principal que nos muestra todos los usuarios
class Home(TemplateView):
    template_name = 'core/core_index.html'

    def get(self, request, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        url = get_absolute_url("api-user:user-listcreate")
        users = requests.get(url)
        context['users'] = users.json()
        return render(request, 'core/core_index.html', context)


# Vista que nos muestra el formulario de creación de usuario en forma de modal
class UserCreateFormView(BSModalCreateView):
    template_name = 'core/user_create.html'
    form_class = UserForm


# Vista que nos muestra el formulario de actualizar usuario en forma de modal
class UserUpdateFormView(BSModalUpdateView):
    template_name = 'core/user_create.html'
    model = User
    form_class = UserUpdateForm


# Vista que nos muestra el login de usuario en forma de modal
class UserLoginFormView(BSModalCreateView):
    template_name = 'core/user_create.html'
    form_class = UserLoginForm


# Función que crea el usuario llamando a las urls de Django rest /api/user
def user_create(request):
    if request.method == "POST":
        form = request.POST
        url = get_absolute_url("api-user:user-listcreate")
        headers = {'Content-type': 'application/json'}
        resp = requests.post(url, data=json.dumps(form), headers=headers)

        if resp.status_code == 201:
            return HttpResponse(resp.content)
        else:
            return HttpResponseBadRequest(resp.content)


# Función que borra el usuario llamando a las urls de Django rest /api/user
def user_delete(request):
    if request.method == "POST":
        pk = request.POST.get('pk', None)
        csrf = request.POST.get('csrfmiddlewaretoken', None)
        url = settings.API_URL + api_reverse("api-user:user-rud", kwargs={'pk': pk})
        cookies = request.COOKIES
        headers = {'Content-type': 'application/json', 'X-CSRFToken': csrf}
        resp = requests.delete(url, headers=headers, cookies=cookies)
        if resp.status_code == 204:
            return HttpResponse(resp)
        else:
            return HttpResponseBadRequest(resp)

# Función que actualiza el usuario llamando a las urls de Django rest /api/user
def user_update(request, pk):
    if request.method == "POST":
        form = request.POST
        url = settings.API_URL + api_reverse("api-user:user-rud", kwargs={'pk': pk})

        cookies = request.COOKIES
        headers = {'Content-type': 'application/json', 'X-CSRFToken': form['csrfmiddlewaretoken']}
        resp = requests.put(url, data=json.dumps(form), headers=headers, cookies=cookies)

        if resp.status_code == 200:
            return HttpResponse(resp)
        else:
            return HttpResponseBadRequest(resp)


# Función que loguea el usuario utilizando las funciones auth de django
def user_login(request):
    if request.method == "POST":
        form = request.POST
        user = authenticate(
            username=form.get('email'),
            password=form.get('password')
        )
        if user is not None:
            if user.is_active:
                try:
                    login(request, user)

                    return HttpResponse("Logged in")
                except Exception as e:
                    print(e)

            return HttpResponseBadRequest("User not active")
        else:
            return HttpResponseBadRequest("User not valid")


# Función que cierra la sesión del usuario utilizando las funciones auth de django
def user_logout(request):
    if request.method == "POST":
        try:
            logout(request)
        except Exception as e:
            print(e)
            return HttpResponseBadRequest(e)

        return HttpResponse("Logout")
