from django.urls import path
from .views import Home, user_create, UserCreateFormView, user_delete, UserLoginFormView, user_login, UserUpdateFormView, user_update, user_logout

app_name = "core-managment"

urlpatterns = [
    path(r'', Home.as_view(), name="home"),
    path(r'registerform/', UserCreateFormView.as_view(), name="user-register-form"),
    path(r'editform/<int:pk>', UserUpdateFormView.as_view(), name="user-edit-form"),
    path(r'edit/<int:pk>', user_update, name="user-update"),
    path(r'loginform/', UserLoginFormView.as_view(), name="user-login-form"),
    path(r'register/', user_create, name="user-register"),
    path(r'delete/', user_delete, name="user-delete"),
    path(r'login/', user_login, name="user-login"),
    path(r'logout/', user_logout, name="user-logout"),
]
