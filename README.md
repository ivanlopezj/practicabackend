# Prueba candidatura Voicemod

Primero de todo decir que hay muchas cosas que me hubiera gustado añadir pero debido al poco tiempo para desarrollarlo y no querer tardar mucho he dejado su funcionamiento básico. Dicho esto vamos con la instalación del proyecto :clapper:.


# Instalación del proyecto

1. Clonamos los datos de proyecto de Bitbucket en nuestro directorio de trabajo.

2.  Creamos un Virtual ENV con python para trabajar con las versiones:

    > python -m venv mivenv

    Activamos nuestro venv para trabajar con el:

    > mienv\Scripts\activate

3. Entramos en el directorio donde hemos clonado el proyecto y ejecutamos el comando:

    > pip install -r requirements.txt

    Esto nos va a instalar todas las dependencias del proyecto.

4. Una vez todo instalado, tendremos que crear la base de datos. En mi caso para esta prueba, he utilizado PostgreSQL. Una vez creada nuestra base de datos, modificaremos los datos en el archivo **settings.py** con las credenciales de la BD para que Django pueda conectarse a ella. Estas son las que he usado yo para desarrollar el proyecto:


		DATABASES  = {
		    'default': {
				    'ENGINE': 'django.db.backends.postgresql',
				    'NAME': 'practicabackend',
				    'USER': 'ilopez',
				    'PASSWORD': 'test123',   
				    'HOST': '127.0.0.1',   
				    'PORT': '5432',
		    }  
	    }
	  
	Y en el mismo archivo settings.py, modificaremos esta línea de código la cual va a especificar la ruta base de nuestro servidor. Poner la URL en la cual nuestro proyecto esté corriendo, en mi caso:
	
		API_URL = "http://127.0.0.1:8000"
	  
5. Una vez hemos introducido los datos de nuestra base de datos, volvemos a la consola para ejecutar los siguientes dos comandos:

    > python manage.py makemigrations

    y

    > python manage.py migrate

    Con esto la base de datos en caso de estar conectada correctamente ya tendrá las tablas creadas.

6. Por último ejecutar el proyecto:

    > python manage.py runserver

Y bueno esto es todo, cualquier cosa no dudéis en poneros en contacto conmigo :thumbsup:. Cualquier cosa que creáis que se podría haber hecho de otra forma mejor no dudeis en comentarmela y así podré aprender de ello!